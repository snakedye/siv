use snui::lens::*;
use snui::widgets::*;
use snui::*;
use snui_adwaita::{adwaita::*, widgets::*};
use snui_wayland::*;

struct Siv {
    path: String,
}

struct View {
    inner: Option<GenericIcon>,
}

lens!(Siv: String, () {() => path});

impl Widget<Siv> for View {
    fn draw_scene(&mut self, scene: scene::Scene, env: &Env) {
        if let Some(icon) = self.inner.as_mut() {
            Widget::<Siv>::draw_scene(icon, scene, env);
        }
    }
    fn event(&mut self, ctx: &mut UpdateContext<Siv>, event: Event, _: &Env) -> Damage {
        ctx.prepare();
        if self.inner.is_none() && event.is_configure() {
            ctx.update()
        }
        Damage::None
    }
    fn update(&mut self, ctx: &mut UpdateContext<Siv>, _: &Env) -> Damage {
        ctx.prepare();
        let path = ctx.path.clone();
        let set = self.inner.is_some();
        self.inner = ctx
            .cache
            .source_cache
            .get(&path)
            .map(GenericIcon::from)
            .ok();
        if self.inner.is_some() != set {
            ctx.window().set_title(path);
            return Damage::Partial;
        }
        Damage::None
    }
    fn layout(&mut self, ctx: &mut LayoutContext<Siv>, bc: &BoxConstraints, env: &Env) -> Size {
        if let Some(inner) = self.inner.as_mut() {
            Widget::<Siv>::layout(inner, ctx, bc, env)
        } else {
            bc.maximum()
        }
    }
}

fn siv() -> impl Widget<Siv> {
    Row!(
        AdwInput::new(())
            .class("frame")
            .padding(10.),
        View { inner: None }.clamp().button(
            |_, ctx: &mut UpdateContext<Siv>, _, pointer| {
                if let Some(serial) = pointer.left_button_click().filter(|_| ctx.window().children() == 0)  {
                    ctx.window()._move(serial);
                }
            }
        ),
    )
    .padding_bottom(10.)
}

fn main() {
    let mut args = std::env::args();
    args.next();
    let path = args.next().unwrap_or_default();
    WaylandClient::init(
        Siv { path: path.clone(), },
        |client, qh| {
            client.set_app_id("siv");
            client.set_environment(AdwaitaTheme::default());
            let window = AdwWindow::new(
                AdwHeader::new((), Label::from("siv").class("title-3")),
                siv()
            );
            let mut view = client.create_window(window, qh);
            view.set_min_size(300, 300);
        },
    )
}
